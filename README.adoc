= Lana Perkins's Lab Notebook

Visit the live Antora lab notebook https://lana-formal-verification-and-ai-lanaperkins-6d9c5bffe864266d474.gitlab.io[here]

This is based on the most recent commit in the `master` branch. 

== Documentation Information
    * This repository contains information on Lana Perkins's research on LLMs and Formal Verification. This contains:
    ** LLM Research
    ** Formal Verification Research
    * Currently this notebook contains a practice repository, but it will be changed at a later date. This main repository is the linker.
    ** One repository contains my notes 
    ** The other contains a practice Antora notebook, which will be swapped out with code later.
    